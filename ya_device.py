import signal
import paho.mqtt.client as mqtt
import time

MQTTS_SERVER_HOST = "mqtt.cloud.yandex.net"
MQTTS_SERVER_PORT = 8883
MQTTS_DEVICE_ID = "are9a9pfrmibasegg1gf"
MQTTS_REGISTER_ID = "aree8n5tpp5uc0bf4igg"
TOPIC = "$registries/%s/events" % MQTTS_REGISTER_ID

MQTT_CLIENT = None


def mqtt_client_on_connect_handler(client, userdata, flags, rc):
    global MQTTS_SERVER_HOST
    global MQTTS_SERVER_PORT
    if rc == 0:
        print("server %s:%s (deviceId:%s) connected" % (MQTTS_SERVER_HOST, str(MQTTS_SERVER_PORT), MQTTS_DEVICE_ID))
    if rc == 5:
        print("Authorization to server %s:%s has failure" % (MQTTS_SERVER_HOST, str(MQTTS_SERVER_PORT)))


def mqtt_client_init():
    global MQTTS_DEVICE_ID
    client = mqtt.Client(MQTTS_DEVICE_ID)
    client.on_connect = mqtt_client_on_connect_handler
    client.tls_set(ca_certs="yandexRootCA.crt", certfile="cert_device1.pem", keyfile="key_device1.pem")
    return client


def mqtt_client_connect(client):
    global MQTTS_SERVER_HOST
    global MQTTS_SERVER_PORT
    client.connect(MQTTS_SERVER_HOST, MQTTS_SERVER_PORT, 60)
    client.loop_start()


def shutdown_handler():
    global MQTT_CLIENT
    MQTT_CLIENT.disconnect()


if __name__ == '__main__':
    MQTT_CLIENT = mqtt_client_init()
    mqtt_client_connect(MQTT_CLIENT)
    signal.signal(signal.SIGINT, shutdown_handler)  # KeyboardInterrupt
    signal.signal(signal.SIGTERM, shutdown_handler)  # Kill
    try:
        cnt = 0
        while True:
            time.sleep(3)
            cnt += 1
            MQTT_CLIENT.publish(topic=TOPIC, payload="publish_counter = {}".format(cnt))
    except Exception:
        print("mqtt device disconnecting...")
        shutdown_handler()
